import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ListWritersComponent } from './components/list-writers/list-writers.component';
import { DetailWriterComponent } from './components/detail-writer/detail-writer.component';

@NgModule({
  declarations: [
    AppComponent,
    ListWritersComponent,
    DetailWriterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
