export class Book {
    id: number
    title: string
    datePublished: number
    image: string
    writer: number

    constructor(id: number, title: string, datePublished: number, image: string,writer: number) {
        this.id = id
        this.title = title
        this.datePublished = datePublished
        this.image = image
        this.writer = writer
    }
}