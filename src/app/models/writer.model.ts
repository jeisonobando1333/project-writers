export class Writer {
    id: number
    name: string
    lastName: string
    image: string
    country:string

    constructor(id: number, name: string, lastName: string, image: string, country:string) {
        this.id = id
        this.name = name
        this.lastName = lastName
        this.image = image
        this.country = country
   }
}