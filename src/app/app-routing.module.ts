import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailWriterComponent } from './components/detail-writer/detail-writer.component';
import { ListWritersComponent } from './components/list-writers/list-writers.component';

const routes: Routes = [

  {
    path: '', pathMatch: 'full', redirectTo: '/writers'
  },
 
  {
    path: 'writers',
    component: ListWritersComponent
  },
  {
    path: 'writers/:id',
    component: DetailWriterComponent
  },
  {
    path: '**',
    redirectTo: 'writers'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
