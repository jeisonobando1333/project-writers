import { Component, OnInit } from '@angular/core';
import { Writer } from 'src/app/models/writer.model';
import { WriterService } from 'src/app/services/writer.service';

@Component({
  selector: 'app-list-writers',
  templateUrl: './list-writers.component.html',
  styleUrls: ['./list-writers.component.sass']
})
export class ListWritersComponent implements OnInit {
  country: string
  countries = ['reino unido', 'españa', 'chile', 'españa', 'canadá'];
  writers: Writer[]

  constructor(private writerService:WriterService) { 
  }

  ngOnInit() : void{ 
    this.writers = this.writerService.getAllWriters()
  }

  async filterByCountry($e) {

    if ($e.target.value != "all"){
      this.writers = await this.writerService.filterByCountry($e.target.value);
      return
    }
    this.writers = this.writerService.getAllWriters()
  }




}
