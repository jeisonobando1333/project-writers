import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from 'src/app/models/book.model';
import { Writer } from 'src/app/models/writer.model';
import { WriterService } from 'src/app/services/writer.service';

@Component({
  selector: 'app-detail-writer',
  templateUrl: './detail-writer.component.html',
  styleUrls: ['./detail-writer.component.sass']
})
export class DetailWriterComponent implements OnInit {

  detailWriter:Writer;
  booksWriter: Book[]

  constructor(private activatedRoute: ActivatedRoute, private writerService: WriterService) { }

  ngOnInit(): void {
    // console.log()
    this.activatedRoute.params.subscribe( params => {
      this.getWriterById(params.id)
    })
  }

  async getWriterById(id) {
    this.detailWriter = await this.writerService.getWriterById(id)
    this.booksWriter = await this.writerService.getBooksOfWriter(id)
  }
}
