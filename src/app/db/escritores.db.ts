import { Writer } from "../models/writer.model";

export const WRITERS: Writer[] = [
    {
        id: 1,
        name: 'Juan',
        lastName: 'Gómez-Jurado',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Juan_Gomez-Jurado.jpg/220px-Juan_Gomez-Jurado.jpg',
        country: 'españa'
    },
    {
        id: 2,
        name: 'Ken',
        lastName: 'Follet',
        image: 'https://upload.wikimedia.org/wikipedia/commons/4/4c/Ken_Follett_official.jpg',
        country: 'reino unido'
    },
    {
        id: 3,
        name: 'Javier',
        lastName: 'Sierra',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Firma_de_Javier_Sierra_%28Sant_Jordi_2009%29.jpg/220px-Firma_de_Javier_Sierra_%28Sant_Jordi_2009%29.jpg',
        country: 'españa'
    },
    {
        id: 4,
        name: 'Carlos',
        lastName: 'Ruiz Zafón',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Carlos_Ruiz_Zaf%C3%B3n_-_002.jpg/220px-Carlos_Ruiz_Zaf%C3%B3n_-_002.jpg',
        country: 'españa'
    },
    {
        id: 5,
        name: 'Isabel',
        lastName: 'Allende',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Isabel_Allende_-_001.jpg/220px-Isabel_Allende_-_001.jpg',
        country: 'chile'
    },
    {
        id: 6,
        name: 'Joanne',
        lastName: 'Rowling',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/J._K._Rowling_2010.jpg/486px-J._K._Rowling_2010.jpg',
        country: 'reino unido'
    },
    {
        id: 7,
        name: 'Margaret',
        lastName: 'Atwood',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Margaret_Atwood_Eden_Mills_Writers_Festival_2006.jpg/477px-Margaret_Atwood_Eden_Mills_Writers_Festival_2006.jpg',
        country: 'canadá'
    },
    {
        id: 8,
        name: 'Almudena',
        lastName: 'Grandes',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Almudena_Grandes.jpg/400px-Almudena_Grandes.jpg',
        country: 'españa'
    }

]
