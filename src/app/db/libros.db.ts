import { Book } from '../models/book.model';

export const BOOKS: Book[] = [
    {
        id: 1,
        title: 'Cicatriz',
        datePublished: 2015,
        image: 'https://imagessl0.casadellibro.com/a/l/t0/90/9788466657990.jpg',
        writer: 1
    },
    {
        id: 2,
        title: 'El Paciente',
        datePublished: 2014,
        image: 'http://www.elplacerdelalectura.com/wp-content/uploads/2014/01/el-paciente-gomez-jurado.jpg',
        writer: 1
    },
    {
        id: 3,
        title: 'La leyenda del ladrón',
        datePublished: 2012,
        image: 'http://2.bp.blogspot.com/-Z6w2vz_kIXc/U7QWbEPCvUI/AAAAAAAAX3w/GRaSK77clv0/s1600/lldl+(1).jpg',
        writer: 1
    },
    {
        id: 4,
        title: 'Los pilares de la tierra',
        datePublished: 1989,
        image: 'https://images-na.ssl-images-amazon.com/images/I/61ZkCOQrryL._SX327_BO1,204,203,200_.jpg',
        writer: 2
    },
    {
        id: 5,
        title: 'En busca de la edad de oro',
        datePublished: 2000,
        image: 'https://imagessl5.casadellibro.com/a/l/t0/15/9788483465615.jpg',
        writer: 3
    },
    {
        id: 6,
        title: 'El ángel perdido',
        datePublished: 2011,
        image: 'https://imagessl6.casadellibro.com/a/l/t0/56/9788408099956.jpg',
        writer: 3
    },
    {
        id: 7,
        title: 'El prisionero del cielo',
        datePublished: 2011,
        image: 'https://static.fnac-static.com/multimedia/ES/images_produits/ES/ZoomPE/4/2/8/9788408105824/tsp20121116141549/El-prisionero-del-cielo.jpg',
        writer: 4
    },
    {
        id: 8,
        title: 'El laberinto de los espíritus',
        datePublished: 2016,
        image: 'https://imagessl1.casadellibro.com/a/l/t0/81/9788408163381.jpg',
        writer: 4
    },
    {
        id: 9,
        title: 'La sombra del viento',
        datePublished: 2001,
        image: 'https://imagessl5.casadellibro.com/a/l/t0/45/9788408043645.jpg',
        writer: 4
    },
    {
        id: 10,
        title: 'El juego de Ripper',
        datePublished: 2014,
        image: 'https://imagessl8.casadellibro.com/a/l/t0/58/9788401342158.jpg',
        writer: 5
    },
    {
        id: 11,
        title: 'Harry Potter y la órden del Fénix',
        datePublished: 2003,
        image: 'https://imagessl1.casadellibro.com/a/l/t0/21/9788498383621.jpg',
        writer: 6
    },
    {
        id: 12,
        title: 'Quidditch a través de los tiempos',
        datePublished: 2001,
        image: 'https://imagessl3.casadellibro.com/a/l/t0/93/9788498382693.jpg',
        writer: 6
    },
    {
        id: 13,
        title: 'Una vacante imprevista',
        datePublished: 2012,
        image: 'https://imagessl5.casadellibro.com/a/l/t0/25/9788498384925.jpg',
        writer: 6
    },
    {
        id: 14,
        title: 'Por último, el corazón',
        datePublished: 2015,
        image: 'https://http2.mlstatic.com/libro-por-ultimo-el-corazon-margaret-atwood--D_NQ_NP_350825-MLU25487204910_042017-F.jpg',
        writer: 7
    },
    {
        id: 15,
        title: 'La semilla de la bruja',
        datePublished: 2016,
        image: 'https://imagessl4.casadellibro.com/a/l/t0/04/9788426404404.jpg',
        writer: 7
    },
    {
        id: 16,
        title: 'Las tres bodas de Manolita',
        datePublished: 2014,
        image: 'https://imagessl9.casadellibro.com/a/l/t0/49/9788490660249.jpg',
        writer: 8
    },
    {
        id: 17,
        title: 'Los besos en el pan',
        datePublished: 2015,
        image: 'https://images-na.ssl-images-amazon.com/images/I/91tFyORVBOL.jpg',
        writer: 8
    },
    {
        id: 18,
        title: 'Los pacientes del doctor García',
        datePublished: 2017,
        image: 'http://blogs.elcorreo.com/divergencias/wp-content/uploads/sites/3/2017/09/los-pacientes-del-doctor-garcia_almudena-grandes_.jpg',
        writer: 8
    }
]
