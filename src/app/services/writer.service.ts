import { Injectable } from '@angular/core';
import { WRITERS } from '../db/escritores.db';
import { BOOKS } from '../db/libros.db';
import { Book } from '../models/book.model';
import { Writer } from '../models/writer.model';

@Injectable({
  providedIn: 'root'
})
export class WriterService {
  
  writers: Writer[] = WRITERS;
  books: Book[] = BOOKS;

  constructor() { }

  getAllWriters(): Writer[] {
    return this.writers
  }

  filterByCountry(country: string): Promise<Writer[]> {
    return new Promise( (resolve, reject) => {
      const writer = this.writers.filter( writer => writer.country === country)
      resolve(writer)

    })
  }

  getWriterById(id: any): Promise<Writer> {
    return new Promise( resolve => {
      const writer = this.writers.find( writer => writer.id == id)
      resolve(writer)
    })
  }

  getBooksOfWriter(id: any): Promise<Book[]> {
    return new Promise( resolve => {
      const booksWriter = this.books.filter(book => book.writer == id)
      resolve(booksWriter)

    })
  }
}
